let
  inherit (import <nixpkgs> {})
    lib
    stdenv
    cargo
    clang
    rustc
    rustfmt
  ;
in

stdenv.mkDerivation rec {
  name = "unit.rs";

  nativeBuildInputs = [
    cargo
    clang
    rustc
    rustfmt
  ];
}
