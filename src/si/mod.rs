use num::Num;
use std::marker;
use tn;
use UnitSystem;

pub struct SecondQty<N>(N);

pub struct SI<N: Num> {
    _base_num_ty: marker::PhantomData<N>,
}

impl<N: Num> SI<N> {
    pub fn second() -> SecondQty<N> {
        SecondQty(N::one())
    }
}

impl<N: Num> UnitSystem for SI<N> {
    type Duration = SecondQty<N>;
}

impl<N: ::std::ops::Mul> ::std::ops::Mul<N> for SecondQty<N> {
    type Output = SecondQty<tn::Prod<N, N>>;
    fn mul(self, other: N) -> SecondQty<tn::Prod<N, N>> {
        let SecondQty(n) = self;
        SecondQty(n * other)
    }
}
