extern crate num;

#[macro_use]
extern crate typenum as tn;

use std::marker::PhantomData;
use num::Num;
pub use si::SI;

mod si;

pub trait UnitSystem {
    type Duration;
}

pub struct Qty<U: Unit, N: Num> {
    magnitude: N,
    _unit: PhantomData<U>,
}

pub trait Unit {
    type Dim: Dimension;
}

pub trait Dimension {
    type LengthExp: TNRat;
    type MassExp: TNRat;
    type TimeExp: TNRat;
}

pub struct Time;

impl Dimension for Time {
    type LengthExp = tn::Z0;
    type MassExp = tn::Z0;
    type TimeExp = tn::P1;
}

pub struct Second;

impl Unit for Second {
    type Dim = Time;
}

pub trait TNRat {
    type Numerator: tn::Integer;
    type Denominator: tn::Integer;
}

impl<N> TNRat for N
    where N: tn::Integer
{
    type Numerator = N;
    type Denominator = tn::P1;
}

struct TNRatQ<P: tn::Integer, Q: tn::Integer> {
    _numerator: PhantomData<P>,
    _denominator: PhantomData<Q>
}

impl<P: tn::Integer, Q: tn::Integer> TNRat for TNRatQ<P, Q> {
    type Numerator = P;
    type Denominator = Q;
}
